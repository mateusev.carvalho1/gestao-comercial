<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\Tenant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class AuthController extends Controller
{
    use AuthTrait;

    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = auth()->user();
        $tenant = Tenant::find($user->id);

        // all good so return the token
        return response()->json(compact('token', 'user', 'tenant'));
    }

    public function getAuthenticatedUser()
    {
        $response = $this->getUser();
        if ($response['status'] != 200)
            return response()->json([$response['response']], $response['status']);

        $user = $response['response'];
        $tenant = Tenant::find($user->id);

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user', 'tenant'));
    }
}
