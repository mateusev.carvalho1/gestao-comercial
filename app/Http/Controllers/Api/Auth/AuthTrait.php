<?php

namespace App\Http\Controllers\Api\Auth;


use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use JWTAuth;


trait AuthTrait
{
    public function getUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return [
                    'response' => 'user_not_found',
                    'status' => 404
                ];
            }
        } catch (TokenExpiredException $e) {
            return [
                'response' => 'token_expired',
                'status' => 500
            ];
        } catch (TokenInvalidException $e) {
            return [
                'response' => 'token_invalid',
                'status' => 500
            ];
        } catch (JWTException $e) {
            return [
                'response' => 'token_absent',
                'status' => 500
            ];
        }

        return [
            'response' => $user,
            'status' => 200
        ];
    }
}