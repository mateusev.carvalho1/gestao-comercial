<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\Tenant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use JWTAuth;

class ProfileController extends Controller
{
    use AuthTrait;

    public function create(Request $request)
    {
        $formData = $request->all();

        $tenant = Tenant::create([
            'company' => $formData['company'],
        ]);

        return $tenant->users()->create([
            'name' => $formData['name'],
            'email' => $formData['email'],
            'phone' => $formData['phone'],
            'password' => Hash::make($formData['password']),
        ]);
    }

    public function update(Request $request)
    {
        $formData = $request->all();

        $response = $this->getUser();
        if ($response['status'] != 200)
            return response()->json([$response['response']], $response['status']);

        $user = $response['response'];
        $user->update($formData);

        return response()->json(compact('user'));
    }


}
