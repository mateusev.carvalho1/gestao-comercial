<?php
namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestBank;
use App\Models\Bank;
use App\Http\Controllers\Controller;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = Bank::all();
        return response()->json(compact('banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestBank $request)
    {
        $conta = Bank::create($request->all());
        if ($conta)
            return response()->json(compact('conta'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bank = Bank::find($id);
        if (!$bank)
            return response()->json('not_found', 404);

        return response()->json(compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestBank $request, $id)
    {
        $bank = Bank::find($id);
        if (!$bank)
            return response()->json('not_found', 404);

        $bank->update($request->all());
        return response()->json(compact('bank'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = Bank::find($id);
        if (!$bank)
            return response()->json('not_found', 404);

        $bank->delete();
        return response()->json([], 204);
    }
}
