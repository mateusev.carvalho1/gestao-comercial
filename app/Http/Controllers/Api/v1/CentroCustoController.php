<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestCentroCusto;
use App\Models\CentroCusto;
use App\Http\Controllers\Controller;

class CentroCustoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centroCustos = CentroCusto::all();
        return response()->json(compact('centroCustos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestCentroCusto $request)
    {
        $centroCusto = CentroCusto::create($request->all());
        if ($centroCusto)
            return response()->json(compact('centroCusto'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $centroCusto = CentroCusto::find($id);
        if (!$centroCusto)
            return response()->json('not_found', 404);

        return response()->json(compact('centroCusto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestCentroCusto $request, $id)
    {
        $centroCusto = CentroCusto::find($id);
        if (!$centroCusto)
            return response()->json('not_found', 404);

        $centroCusto->update($request->all());
        return response()->json(compact('centroCusto'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $centroCusto = CentroCusto::find($id);
        if (!$centroCusto)
            return response()->json('not_found', 404);

        $centroCusto->delete();
        return response()->json([], 204);
    }
}
