<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\RequestClient;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{

    private $totalPaginate = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Client $client)
    {
        $clients = $client->getResults($request->all(), $this->totalPaginate);

        return response()->json(compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestClient $request)
    {
        $formData = $request->all();

        $validatorTypeClient = $request->validateTypeClient($formData['tipo'], $formData['type_client_physicals'],
            $formData['type_client_legals'], $formData['type_client_foreigns']);
        if ($validatorTypeClient)
            return response()->json($validatorTypeClient, 401);

        if ($formData['address_clients']) {
            $validatorAdresses = $request->validateAddress($formData['address_clients']);
            if ($validatorAdresses)
                return response()->json($validatorAdresses, 401);
        }

        if ($formData['contact_clients']) {
            $validatorContacts = $request->validateContacts($formData['contact_clients']);
            if ($validatorContacts)
                return response()->json($validatorContacts, 401);
        }

        $client = Client::create($formData);
        if ($client) {
            if ($formData['tipo'] === 'fisica') {
                $client->typeClientPhysicals()->create($formData['type_client_physicals']);
            }
            if ($formData['tipo'] === 'juridica') {
                $client->typeClientLegals()->create($formData['type_client_legals']);
            }
            if ($formData['tipo'] === 'estrangeiro') {
                $client->typeClientForeigns()->create($formData['type_client_foreigns']);
            }

            foreach ($formData['address_clients'] as $address) {
                $client->addressClients()->create($address);
            }
            foreach ($formData['contact_clients'] as $contact) {
                $client->contactClients()->create($contact);
            }
        }

        return response()->json(compact('client'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::with(['addressClients', 'contactClients', 'typeClientPhysicals',
            'typeClientLegals', 'typeClientForeigns'])->where('id', $id)->first();
        if (!$client)
            return response()->json(['error', 'not_found'], 404);

        return response()->json($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestClient $request, $id)
    {
        $formData = $request->all();

        $validatorTypeClient = $request->validateTypeClient($formData['tipo'], $formData['type_client_physicals'],
            $formData['type_client_legals'], $formData['type_client_foreigns']);
        if ($validatorTypeClient)
            return response()->json($validatorTypeClient, 401);

        if ($formData['address_clients']) {
            $validatorAdresses = $request->validateAddress($formData['address_clients']);
            if ($validatorAdresses)
                return response()->json($validatorAdresses, 401);
        }

        if ($formData['contact_clients']) {
            $validatorContacts = $request->validateContacts($formData['contact_clients']);
            if ($validatorContacts)
                return response()->json($validatorContacts, 401);
        }

        $client = Client::find($id);
        if (!$client)
            return response()->json(['error', 'not_found'], 404);

        $isUpdate = $client->update($formData);
        if ($isUpdate) {
            if ($formData['tipo'] === 'fisica') {
                $client->typeClientPhysicals()->delete();
                $client->typeClientPhysicals()->create($formData['type_client_physicals']);
            }
            if ($formData['tipo'] === 'juridica') {
                $client->typeClientLegals()->delete();
                $client->typeClientLegals()->create($formData['type_client_legals']);
            }
            if ($formData['tipo'] === 'estrangeiro') {
                $client->typeClientForeigns()->delete();
                $client->typeClientForeigns()->create($formData['type_client_foreigns']);
            }

            $client->addressClients()->delete();
            foreach ($formData['address_clients'] as $address) {
                $client->addressClients()->create($address);
            }

            $client->contactClients()->delete();
            foreach ($formData['contact_clients'] as $contact) {
                $client->contactClients()->create($contact);
            }
        }
        return response()->json($client, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        if (!$client)
            return response()->json(['error', 'not_found'], 404);

        $client->delete();
        return response()->json([], 204);
    }
}
