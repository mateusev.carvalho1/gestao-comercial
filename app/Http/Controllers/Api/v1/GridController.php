<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestGrid;
use App\Models\Grid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GridController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grids = Grid::with('variations')->get();
        return response()->json(compact('grids'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestGrid $request)
    {
        $formData = $request->all();

        if ($formData['variations']) {
            $validatorVariation = $request->validateVariations($formData['variations']);
            if ($validatorVariation)
                return response()->json($validatorVariation, 401);
        }

        $grid = Grid::create($formData);
        if ($grid) {
            foreach ($formData['variations'] as $variation) {
                $grid->variations()->create($variation);
            }
        }

        return response()->json(compact('grid'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $grid = Grid::with('variations')->where('id', $id)->first();
        if (!$grid)
            return response()->json(['error', 'not_found'], 404);

        return response()->json(compact('grid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestGrid $request, $id)
    {
        $formData = $request->all();

        if ($formData['variations']) {
            $validatorVariation = $request->validateVariations($formData['variations']);
            if ($validatorVariation)
                return response()->json($validatorVariation, 401);
        }

        $grid = Grid::find($id);
        if (!$grid)
            return response()->json(['error', 'not_found'], 404);

        $isUpdate = $grid->update($formData);
        if ($isUpdate) {
            $grid->variations()->delete();
            foreach ($formData['variations'] as $variation) {
                $grid->variations()->create($variation);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grid = Grid::find($id);
        if (!$grid)
            return response()->json(['error', 'not_found'], 404);

        $grid->delete();
        return response()->json([], 204);
    }
}
