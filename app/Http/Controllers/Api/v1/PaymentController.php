<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestPayment;
use App\Models\Payment;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::with('bank')->get();
        return response()->json(compact('payments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestPayment $request)
    {
        $payment = Payment::create($request->all());
        if ($payment)
            return response()->json(compact('payment'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = Payment::find($id);
        if (!$payment)
            return response()->json('not_found', 404);

        return response()->json(compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestPayment $request, $id)
    {
        $payment = Payment::find($id);
        if (!$payment)
            return response()->json('not_found', 404);

        $payment->update($request->all());
        return response()->json(compact('payment'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = Payment::find($id);
        if (!$payment)
            return response()->json('not_found', 404);

        $payment->delete();
        return response()->json([], 204);
    }
}
