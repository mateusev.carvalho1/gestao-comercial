<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestPlanoContas;
use App\Models\PlanoConta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanoContaController extends Controller
{

    private $totalPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, PlanoConta $conta)
    {
        $planoContas = $conta->getResults($request->all(), $this->totalPage);
        return response()->json(compact('planoContas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestPlanoContas $request)
    {
        $planoConta = PlanoConta::create($request->all());
        if ($planoConta)
            return response()->json(compact('planoConta'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $planoConta = PlanoConta::find($id);
        if (!$planoConta)
            return response()->json('not_found', 404);

        return response()->json(compact('planoConta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $planoConta = Payment::find($id);
        if (!$planoConta)
            return response()->json('not_found', 404);

        $planoConta->update($request->all());
        return response()->json(compact('planoConta'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $planoConta = PlanoConta::find($id);
        if (!$planoConta)
            return response()->json('not_found', 404);

        $planoConta->delete();
        return response()->json([], 204);
    }
}
