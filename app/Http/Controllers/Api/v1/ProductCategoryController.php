<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestProductCategory;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{


    private $totalPaginate = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->get('select') == 'select')
            $category = ProductCategory::all();
        else {
            $category = ProductCategory::paginate($this->totalPaginate);
        }
        return response()->json(compact('category'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestProductCategory $request)
    {
        $category = ProductCategory::create($request->all());
        if ($category)
            return response()->json(compact('category'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = ProductCategory::find($id);
        if (!$category)
            return response()->json('unidade_not_found', 404);

        return response()->json(compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = ProductCategory::find($id);
        if (!$category)
            return response()->json('unidade_not_found', 404);

        $category->update($request->all());
        return response()->json(compact('category'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = ProductCategory::find($id);
        if (!$category)
            return response()->json('unidade_not_found', 404);

        $category->delete();
        return response()->json(compact([], 204));
    }
}
