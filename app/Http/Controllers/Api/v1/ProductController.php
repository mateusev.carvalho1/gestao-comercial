<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestProduct;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    private $totalPaginate = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Product $product)
    {
        $products = $product->getResults($request->all(), $this->totalPaginate);
        return response()->json(compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestProduct $request)
    {
        $formData = $request->all();

        foreach ($formData['product_grids'] as $key => $product_grid) {
            $formData['product_grids'][$key] = json_decode($product_grid, true);
        }

        $validatorGrids = $request->validateGrids($formData['product_grids']);
        if ($validatorGrids) {
            return response()->json($validatorGrids, 401);
        }

        if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
            $name = kebab_case($formData['nome']) . time();
            $extension = $request->imagem->extension();
            $nameImage = "{$name}.{$extension}";
            $formData['imagem'] = $nameImage;

            $upload = $request->imagem->storeAs('products', $nameImage);
            if (!$upload)
                return response()->json(['error' => 'error_upload'], 500);
        }

        $createProduct = Product::create($formData);
        if ($createProduct) {
            foreach ($formData['product_grids'] as $product_grid) {
                $createGrid = $createProduct->productGrids()->create($product_grid);
                if ($formData['possui_variacao']) {
                    foreach ($product_grid['variations'] as $product_variation) {
                        $product_variation['product_grid_id'] = $createGrid->id;
                        ProductVariation::create($product_variation);
                    }
                }
            }
        }

        return response()->json($createProduct, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with('productCategory', 'unity', 'provider', 'productGrids.variations.grid.variations')
            ->where('id', $id)->first();
        if (!$product)
            return response()->json(['error', 'not_found'], 404);

        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestProduct $request, $id)
    {
        $formData = $request->all();

        foreach ($formData['product_grids'] as $key => $product_grid) {
            $formData['product_grids'][$key] = json_decode($product_grid, true);
        }

        $validatorGrids = $request->validateGrids($formData['product_grids']);
        if ($validatorGrids) {
            return response()->json($validatorGrids, 401);
        }

        $product = Product::find($id);
        if (!$product)
            return response()->json(['error', 'not_found'], 404);


        if (!isset($formData['imagem']) && $formData['stringImagem'] == null) {
            $formData['imagem'] = '';
            $this->deleteImage($product->imagem);
        }

        if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {

            if ($product->imagem) {
                $this->deleteImage($product->imagem);
            }

            $name = kebab_case($formData['nome']) . time();
            $extension = $request->imagem->extension();
            $nameImage = "{$name}.{$extension}";
            $formData['imagem'] = $nameImage;

            $upload = $request->imagem->storeAs('products', $nameImage);
            if (!$upload)
                return response()->json(['error' => 'error_upload'], 500);
        }

        $isUpdate = $product->update($formData);
        if ($isUpdate) {

            $product->productGrids()->delete();
            foreach ($formData['product_grids'] as $product_grid) {
                $createGrid = $product->productGrids()->create($product_grid);
                if ($formData['possui_variacao']) {
                    foreach ($product_grid['variations'] as $product_variation) {
                        $product_variation['product_grid_id'] = $createGrid->id;
                        ProductVariation::create($product_variation);
                    }
                }
            }

        }
        return response()->json($product, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if (!$product)
            return response()->json(['error', 'not_found'], 404);

        $this->deleteImage($product->imagem);
        $product->delete();
    }

    private function deleteImage($imagem)
    {
        if (Storage::exists("products/{$imagem}")) {
            Storage::delete("products/{$imagem}");
        }
    }

}
