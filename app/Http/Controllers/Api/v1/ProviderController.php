<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestProvider;
use App\Models\Provider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProviderController extends Controller
{

    private $totalPaginate = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Provider $provider)
    {
        $providers = $provider->getResults($request->all(), $this->totalPaginate);
        return response()->json(compact('providers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestProvider $request)
    {
        $formData = $request->all();

        $validatorTypeProvider = $request->validateTypeProvider($formData['tipo'], $formData['type_provider_physical'],
            $formData['type_provider_legal'], $formData['type_provider_foreign']);
        if ($validatorTypeProvider)
            return response()->json($validatorTypeProvider, 401);

        if ($formData['provider_adresses']) {
            $validatorAdresses = $request->validateAddress($formData['provider_adresses']);
            if ($validatorAdresses)
                return response()->json($validatorAdresses, 401);
        }

        if ($formData['provider_contacts']) {
            $validatorContacts = $request->validateContacts($formData['provider_contacts']);
            if ($validatorContacts)
                return response()->json($validatorContacts, 401);
        }

        $provider = Provider::create($formData);
        if ($provider) {
            if ($formData['tipo'] === 'fisica') {
                $provider->typeProviderPhysical()->create($formData['type_provider_physical']);
            }
            if ($formData['tipo'] === 'juridica') {
                $provider->typeProviderLegal()->create($formData['type_provider_legal']);
            }
            if ($formData['tipo'] === 'estrangeiro') {
                $provider->typeProviderForeign()->create($formData['type_provider_foreign']);
            }

            foreach ($formData['provider_adresses'] as $address) {
                $provider->providerAdresses()->create($address);
            }
            foreach ($formData['provider_contacts'] as $contact) {
                $provider->providerContacts()->create($contact);
            }
        }

        return response()->json(compact('provider'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::with(['providerAdresses', 'providerContacts', 'typeProviderPhysical',
            'typeProviderLegal', 'typeProviderForeign'])->where('id', $id)->first();
        if (!$provider)
            return response()->json(['error', 'not_found'], 404);

        return response()->json($provider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestProvider $request, $id)
    {
        $formData = $request->all();

        $validatorTypeProvider = $request->validateTypeProvider($formData['tipo'], $formData['type_provider_physical'],
            $formData['type_provider_legal'], $formData['type_provider_foreign']);
        if ($validatorTypeProvider)
            return response()->json($validatorTypeProvider, 401);

        if ($formData['provider_adresses']) {
            $validatorAdresses = $request->validateAddress($formData['provider_adresses']);
            if ($validatorAdresses)
                return response()->json($validatorAdresses, 401);
        }

        if ($formData['provider_contacts']) {
            $validatorContacts = $request->validateContacts($formData['provider_contacts']);
            if ($validatorContacts)
                return response()->json($validatorContacts, 401);
        }

        $provider = Provider::find($id);
        if (!$provider)
            return response()->json(['error', 'not_found'], 404);

        $isUpdate = $provider->update($formData);
        if ($isUpdate) {
            if ($formData['tipo'] === 'fisica') {
                $provider->typeProviderPhysical()->delete();
                $provider->typeProviderPhysical()->create($formData['type_provider_physical']);
            }
            if ($formData['tipo'] === 'juridica') {
                $provider->typeProviderLegal()->delete();
                $provider->typeProviderLegal()->create($formData['type_provider_legal']);
            }
            if ($formData['tipo'] === 'estrangeiro') {
                $provider->typeProviderForeign()->delete();
                $provider->typeProviderForeign()->create($formData['type_provider_foreign']);
            }

            $provider->providerAdresses()->delete();
            foreach ($formData['provider_adresses'] as $address) {
                $provider->providerAdresses()->create($address);
            }

            $provider->providerContacts()->delete();
            foreach ($formData['provider_contacts'] as $contact) {
                $provider->providerContacts()->create($contact);
            }
        }

        return response()->json(compact('provider'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provider = Provider::find($id);
        if (!$provider)
            return response()->json(['error', 'not_found'], 404);

        $provider->delete();
        return response()->json([], 204);
    }
}
