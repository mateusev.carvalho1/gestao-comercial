<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\RequestUnity;
use App\Models\Unity;
use App\Http\Controllers\Controller;

class UnityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unities = Unity::all();
        return response()->json(compact('unities'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestUnity $request)
    {
        $unity = Unity::create($request->all());
        if ($unity)
            return response()->json(compact('unity'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unity = Unity::find($id);
        if (!$unity)
            return response()->json('unidade_not_found', 404);

        return response()->json(compact('unity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestUnity $request, $id)
    {
        $unity = Unity::find($id);
        if (!$unity)
            return response()->json('unidade_not_found', 404);

        $unity->update($request->all());
        return response()->json(compact('unity'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unity = Unity::find($id);
        if (!$unity)
            return response()->json('unidade_not_found', 404);

        $unity->delete();
        return response()->json(compact([], 204));
    }
}
