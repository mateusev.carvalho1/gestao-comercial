<?php

namespace App\Http\Middleware;

use App\Tenant\ManagerTenant;
use Closure;

class TenantFilesystems
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->setFileSystemRoot();
        return $next($request);
    }

    private function setFileSystemRoot()
    {
        $tenant = app(ManagerTenant::class)->getTenant();
        $path = config('filesystems.disks.tenant.root');

        config(['filesystems.disks.tenant.root' => $path . "/{$tenant->uuid}"]);
    }
}
