<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class RequestClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo'          => 'required',
            'name'          => 'required|min:3|max:60',
            'email'         => 'nullable|e-mail',
            'site'          => 'max:100',
            'celular'       => 'max:15',
            'telefone'      => 'max:15',
            'observacao'    => 'max:5000',
        ];
    }

    public function validateAddress($adresses)
    {
        foreach ($adresses as $key => $address) {
            $validator = Validator::make($address, [
                'tipo'          => 'max:20',
                'cep'           => 'max:10',
                'logradouro'    => 'max:190',
                'numero'        => 'nullable|integer',
                'complemento'   => 'max:190',
                'bairro'        => 'max:80',
                'cidade'        => 'max:100',
                'uf'            => 'max:2',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                $location = 'adresses';
                return compact('errors', 'location', 'key');
            }
        }
        return false;
    }

    public function validateContacts($contacts)
    {
        foreach ($contacts as $key => $contact) {
            $validator = Validator::make($contact, [
                'tipo'          => 'max:30',
                'contato'       => 'max:100',
                'name'          => 'max:60',
                'cargo'         => 'max:60',
                'observacao'    => 'max:190',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                $location = 'contacts';
                return compact('errors', 'key', 'location');
            }
        }
        return false;
    }

    public function validateTypeClient($typeClient, $typePhysicals, $typeLegals, $typeForeigns)
    {
        if ($typeClient == 'fisica'){
            $validator = Validator::make($typePhysicals, [
                'cpf'               => 'max:14',
                'rg'                => 'max:20',
                'data_nascimento'   => 'nullable|date'
            ]);
            if($validator->fails()){
                $errors = $validator->errors();
                $location = 'physical';
                return compact('errors', 'location');
            }else {
                return false;
            }
        }

        elseif ($typeClient == 'juridica') {
            $validator = Validator::make($typeLegals, [
                'cnpj'                  => 'max:20',
                'razao_social'          => 'max:80',
                'inscricao_estadual'    => 'max:40',
                'inscricao_municipal'   => 'max:40',
                'suframa'               => 'max:40',
                'tipo_contribuinte'     => 'max:20',
                'responsavel'           => 'max:60',
            ]);
            if($validator->fails()){
                $errors = $validator->errors();
                $location = 'legals';
                return compact('errors', 'location');
            }else {
                return false;
            }
        }

        elseif ($typeClient == 'estrangeiro'){
            $validator = Validator::make($typeForeigns, [
                'documento' => 'max:190',
            ]);
            if($validator->fails()){
                $errors = $validator->errors();
                $location = 'foreigns';
                return compact('errors', 'location');
            }else {
                return false;
            }
        }

    }
}
