<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class RequestGrid extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:15'
        ];
    }

    public function validateVariations($variations)
    {
        foreach ($variations as $key => $variation) {
            $validator = Validator::make($variation, [
                'name' => 'required|max:15'
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                $location = 'variations';
                return compact('errors', 'location', 'key');
            }
        }
        return false;
    }
}
