<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPayment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id'           => 'required',
            'name'              => 'required|max:30',
            'confirmacao'       => 'required',
            'parcelas'          => 'required|integer',
            'intervalo'         => 'required|integer',
            'primeira_parcela'  => 'required|integer',
            'taxa_banco'        => 'required|numeric',
            'taxa_operadora'    => 'required|numeric',
            'multa'             => 'required|numeric',
            'gerar_boleto'      => 'required',
        ];
    }
}
