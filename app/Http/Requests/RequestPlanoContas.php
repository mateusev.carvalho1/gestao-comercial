<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPlanoContas extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tenant_id'             => 'required',
            'payment_id'            => 'required',
            'bank_id'               => 'required',
            'centro_custo_id'       => 'nullable',
            'name'                  => 'required|max:30',
            'ocorrencia'            => 'required|max:20',
            'proximo_vencimento'    => 'required|date',
            'quantas_vezes'         => 'required|max:20',
            'data_termino'          => 'nullable|date',
            'quantas'               => 'nullable|integer',
            'valor'                 => 'decimal',
            'observacao'            => 'nullable|max:1000',
        ];
    }
}
