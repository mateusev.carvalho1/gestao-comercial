<?php

namespace App\Http\Requests;

use App\Rules\Tenant\TenantUnique;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class RequestProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_category_id'   => 'required',
            'unity_id'              => 'required',
            'nome'                  => ['required', 'max:190', 'min:3', new TenantUnique('products', $this->segment(4))],
            'codigo_interno'        => ['required', 'max:30', new TenantUnique('products', $this->segment(4))],
            'codigo_barras'         => 'max:30',
            'movimenta_estoque'     => 'required|boolean',
            'emitir_nf'             => 'required|boolean',
            'possui_variacao'       => 'required|boolean',
            'possui_juros'          => 'required|boolean',
            'ativo'                 => 'required|boolean',
            'juros'                 => 'numeric',
            'peso'                  => 'numeric',
            'largura'               => 'numeric',
            'altura'                => 'numeric',
            'cumprimento'           => 'numeric',
            'comissao'              => 'numeric',
            'imagem'                => 'nullable|image',
            'observacao'            => 'max:10000'
        ];
    }

    public function validateGrids($grids)
    {
        foreach ($grids as $key => $grid) {
            $requiredCod = 'required';
            $id = isset($grid['id']) ? $grid['id'] : '';

            if ($grid['variations'] == null)
                $requiredCod = 'nullable';

            $validator = Validator::make($grid, [
                'codigo_identificacao'  => [$requiredCod, new TenantUnique('product_grids', $id)],
                'estoque_minimo'        => 'required|integer',
                'estoque_maximo'        => 'required|integer',
                'estoque_atual'         => 'required|integer',
                'valor_custo'           => 'numeric',
                'valor_despesas'        => 'numeric',
                'valor_custo_final'     => 'numeric',
                'valor_venda'           => 'required|numeric',
                'porcentagem_lucro'     => 'numeric',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                $location = 'grids';
                return compact('errors', 'location', 'key');
            }

            if (isset($grid['variations'])) {
                foreach ($grid['variations'] as $key => $variation) {
                    $validatorVariation = Validator::make($variation, [
                        'variation_id'  => 'required',
                    ]);
                    if ($validatorVariation->fails()) {
                        $errors = $validatorVariation->errors();
                        $location = 'variations';
                        return compact('errors', 'key', 'location');
                    }
                }
            } //end if

        } // end foreach
        return false;
    }
}
