<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class AddressClient extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'tipo', 'cep', 'logradouro', 'numero', 'complemento', 'bairro', 'cidade', 'uf'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
