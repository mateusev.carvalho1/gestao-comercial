<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class AddressProvider extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'tipo', 'cep', 'logradouro', 'numero', 'complemento', 'bairro', 'cidade', 'uf'
    ];
}
