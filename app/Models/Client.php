<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'tipo', 'name', 'email', 'site', 'celular', 'telefone', 'observacao'
    ];

    public function getResults($data, $total)
    {
        if (!isset($data['tipo']) && !isset($data['cpf_cnpj']) && !isset($data['name']) && !isset($data['email'])
            && !isset($data['telefone']) && !isset($data['cidade']) && !isset($data['uf'])) {
            return $this->orderBy('id', 'DESC')->paginate($total);
        }

        if (!isset($data['tipo'])) {
            return $this->where(function ($query) use ($data) {
                if (isset($data['tipo'])) {
                    $query->where('tipo', 'LIKE', "%{$data['tipo']}%");
                }
                if (isset($data['name'])) {
                    $query->where('name', 'LIKE', "%{$data['name']}%");
                }
                if (isset($data['email'])) {
                    $query->where('email', 'LIKE', "%{$data['email']}%");
                }
                if (isset($data['telefone'])) {
                    $query->where('telefone', 'LIKE', "%{$data['telefone']}%");
                    $query->orWhere('celular', 'LIKE', "%{$data['telefone']}%");
                }
            })->whereHas('addressClients', function ($query) use ($data) {
                if (isset($data['cidade'])) {
                    $query->where('cidade', 'LIKE', "%{$data['cidade']}%");
                }
                if (isset($data['uf'])) {
                    $query->where('uf', 'LIKE', "%{$data['uf']}%");
                }
            })->orderBy('id', 'DESC')->paginate($total);
        }

        if (isset($data['tipo'])) {

            if ($data['tipo'] == 'fisica') {
                $metodo = 'typeClientPhysicals';
                $campo = 'cpf';
            }
            if ($data['tipo'] == 'juridica') {
                $metodo = 'typeClientLegals';
                $campo = 'cnpj';
            }

            if ($data['tipo'] == 'estrangeiro') {
                $metodo = 'typeClientForeigns';
                $campo = 'documento';
            }
            return $this->where(function ($query) use ($data) {
                if (isset($data['tipo'])) {
                    $query->where('tipo', 'LIKE', "%{$data['tipo']}%");
                }
                if (isset($data['name'])) {
                    $query->where('name', 'LIKE', "%{$data['name']}%");
                }
                if (isset($data['email'])) {
                    $query->where('email', 'LIKE', "%{$data['email']}%");
                }
                if (isset($data['telefone'])) {
                    $query->where('telefone', 'LIKE', "%{$data['telefone']}%");
                    $query->orWhere('celular', 'LIKE', "%{$data['telefone']}%");
                }
            })->whereHas('addressClients', function ($query) use ($data) {
                if (isset($data['cidade'])) {
                    $query->where('cidade', 'LIKE', "%{$data['cidade']}%");
                }
                if (isset($data['uf'])) {
                    $query->where('uf', 'LIKE', "%{$data['uf']}%");
                }
            })->whereHas($metodo, function ($query) use ($data, $campo) {
                if (isset($data[$campo])) {
                    $query->where($campo, 'LIKE', "%{$data[$campo]}%");
                }
            })->orderBy('id', 'DESC')->paginate($total);

        }
    }

    public function addressClients()
    {
        return $this->hasMany(AddressClient::class);
    }

    public function contactClients()
    {
        return $this->hasMany(ContactClient::class);
    }

    public function typeClientPhysicals()
    {
        return $this->hasOne(TypeClientPhysical::class);
    }

    public function typeClientLegals()
    {
        return $this->hasOne(TypeClientLegal::class);
    }

    public function typeClientForeigns()
    {
        return $this->hasOne(TypeClientForeign::class);
    }
}
