<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class ContactClient extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'tipo', 'contato', 'name', 'cargo', 'observacao'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
