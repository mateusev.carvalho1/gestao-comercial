<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class ContactProvider extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'tipo', 'contato', 'name', 'cargo', 'observacao'
    ];
}
