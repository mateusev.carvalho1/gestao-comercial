<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class Grid extends Model
{
    use TenantBootTrait;

    protected $fillable = ['name'];

    public function variations()
    {
        return $this->hasMany(Variation::class);
    }
}
