<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use TenantBootTrait;

    protected $fillable = ['bank_id', 'name', 'confirmacao', 'parcelas', 'intervalo', 'primeira_parcela', 'taxa_banco',
        'taxa_operadora', 'multa', 'gerar_boleto'];

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
