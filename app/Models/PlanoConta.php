<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class PlanoConta extends Model
{
    use TenantBootTrait;
    protected $fillable = [
        'payment_id', 'bank_id', 'centro_custo_id', 'name', 'ocorrencia', 'proximo_vencimento', 'quantas_vezes',
        'data_termino', 'quantas', 'valor', 'observacao'
    ];

    public function getResults($data, $total)
    {
        if (!isset($data['name'])) {
            return $this->orderBy('id', 'DESC')->paginate($total);
        }

        return $this->where('name', 'like', "{$data['name']}")->orderBy('id', 'DESC')->paginate($total);
    }
}
