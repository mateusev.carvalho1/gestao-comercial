<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'product_category_id', 'unity_id', 'nome', 'codigo_interno', 'codigo_barras', 'movimenta_estoque', 'emitir_nf',
        'possui_variacao', 'possui_juros', 'ativo', 'juros', 'peso', 'largura', 'altura', 'cumprimento', 'comissao',
        'imagem', 'observacao', 'provider_id'
    ];

    public function getResults($data, $total)
    {
        if (!isset($data['nome']) && !isset($data['codigo_interno']) && !isset($data['codigo_barras'])
            && !isset($data['categoria']) && !isset($data['fornecedor'])) {
            return $this->with('productGrids')->orderBy('id', 'DESC')->paginate($total);
        }

        return $this->where(function ($query) use ($data) {
            if (isset($data['nome']))
                $query->where('nome', 'LIKE', "%{$data['nome']}%");

            if (isset($data['codigo_interno']))
                $query->where('codigo_interno', 'LIKE', "%{$data['codigo_interno']}%");

            if (isset($data['codigo_barras']))
                $query->where('codigo_barras', 'LIKE', "%{$data['codigo_barras']}%");

            if (isset($data['categoria']))
                $query->where('product_category_id', $data['categoria']);

            if (isset($data['fornecedor']))
                $query->where('provider_id', $data['fornecedor']);
        })->with('productGrids')->orderBy('id', 'DESC')->paginate($total);
    }

    public function productGrids()
    {
        return $this->hasMany(ProductGrid::class);
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function unity()
    {
        return $this->belongsTo(Unity::class);
    }
}
