<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use TenantBootTrait;

    protected $fillable = ['name'];
}
