<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class ProductGrid extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'codigo_identificacao', 'estoque_minimo', 'estoque_maximo', 'estoque_atual', 'valor_custo', 'valor_despesas',
        'valor_custo_final', 'valor_venda', 'porcentagem_lucro'
    ];

    public function variations()
    {
        return $this->belongsToMany(Variation::class, 'product_variations');
    }
}
