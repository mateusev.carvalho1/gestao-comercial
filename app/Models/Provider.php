<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'tipo', 'name', 'email', 'site', 'celular', 'telefone', 'observacao'
    ];

    public function getResults($data, $total)
    {
        if (isset($data['select']) && $data['select'] == 'select') {
            return $this->all();
        }

        if (!isset($data['tipo']) && !isset($data['cpf_cnpj']) && !isset($data['name']) && !isset($data['email'])
            && !isset($data['telefone']) && !isset($data['cidade']) && !isset($data['uf'])) {
            return $this->orderBy('id', 'DESC')->paginate($total);
        }

        if (!isset($data['tipo'])) {
            return $this->where(function ($query) use ($data) {
                if (isset($data['tipo'])) {
                    $query->where('tipo', 'LIKE', "%{$data['tipo']}%");
                }
                if (isset($data['name'])) {
                    $query->where('name', 'LIKE', "%{$data['name']}%");
                }
                if (isset($data['email'])) {
                    $query->where('email', 'LIKE', "%{$data['email']}%");
                }
                if (isset($data['telefone'])) {
                    $query->where('telefone', 'LIKE', "%{$data['telefone']}%");
                    $query->orWhere('celular', 'LIKE', "%{$data['telefone']}%");
                }
            })->whereHas('providerAdresses', function ($query) use ($data) {
                if (isset($data['cidade'])) {
                    $query->where('cidade', 'LIKE', "%{$data['cidade']}%");
                }
                if (isset($data['uf'])) {
                    $query->where('uf', 'LIKE', "%{$data['uf']}%");
                }
            })->orderBy('id', 'DESC')->paginate($total);
        }

        if (isset($data['tipo'])) {

            if ($data['tipo'] == 'fisica') {
                $metodo = 'typeProviderPhysical';
                $campo = 'cpf';
            }
            if ($data['tipo'] == 'juridica') {
                $metodo = 'typeProviderLegal';
                $campo = 'cnpj';
            }

            if ($data['tipo'] == 'estrangeiro') {
                $metodo = 'typeProviderForeign';
                $campo = 'documento';
            }
            return $this->where(function ($query) use ($data) {
                if (isset($data['tipo'])) {
                    $query->where('tipo', 'LIKE', "%{$data['tipo']}%");
                }
                if (isset($data['name'])) {
                    $query->where('name', 'LIKE', "%{$data['name']}%");
                }
                if (isset($data['email'])) {
                    $query->where('email', 'LIKE', "%{$data['email']}%");
                }
                if (isset($data['telefone'])) {
                    $query->where('telefone', 'LIKE', "%{$data['telefone']}%");
                    $query->orWhere('celular', 'LIKE', "%{$data['telefone']}%");
                }
            })->whereHas('providerAdresses', function ($query) use ($data) {
                if (isset($data['cidade'])) {
                    $query->where('cidade', 'LIKE', "%{$data['cidade']}%");
                }
                if (isset($data['uf'])) {
                    $query->where('uf', 'LIKE', "%{$data['uf']}%");
                }
            })->whereHas($metodo, function ($query) use ($data, $campo) {
                if (isset($data[$campo])) {
                    $query->where($campo, 'LIKE', "%{$data[$campo]}%");
                }
            })->orderBy('id', 'DESC')->paginate($total);

        }
    }

    public function providerAdresses()
    {
        return $this->hasMany(AddressProvider::class);
    }

    public function providerContacts()
    {
        return $this->hasMany(ContactProvider::class);
    }

    public function typeProviderPhysical()
    {
        return $this->hasOne(TypeProviderPhysical::class);
    }

    public function typeProviderLegal()
    {
        return $this->hasOne(TypeProviderLegal::class);
    }

    public function typeProviderForeign()
    {
        return $this->hasOne(TypeProviderForeign::class);
    }
}
