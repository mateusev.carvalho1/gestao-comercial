<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class TypeClientForeign extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'client_id', 'documento'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
