<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class TypeClientLegal extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'client_id', 'cnpj', 'razao_social', 'inscricao_estadual', 'suframa', 'tipo_contribuinte', 'responsavel'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
