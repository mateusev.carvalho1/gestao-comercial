<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class TypeClientPhysical extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'client_id', 'cpf', 'rg', 'data_nascimento'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
