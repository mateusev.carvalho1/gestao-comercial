<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class TypeProviderForeign extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'documento'
    ];
}
