<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class TypeProviderLegal extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'cnpj', 'razao_social', 'inscricao_estadual', 'suframa', 'tipo_contribuinte', 'responsavel'
    ];
}
