<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class TypeProviderPhysical extends Model
{
    use TenantBootTrait;

    protected $fillable = [
        'cpf', 'rg', 'data_nascimento'
    ];
}
