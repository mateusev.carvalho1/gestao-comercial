<?php

namespace App\Models;

use App\Tenant\TenantBootTrait;
use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
    use TenantBootTrait;

    protected $fillable = ['name'];

    public function productGrids()
    {
        return $this->belongsToMany(ProductGrid::class, 'product_variations');
    }

    public function grid()
    {
        return $this->belongsTo(Grid::class);
    }
}
