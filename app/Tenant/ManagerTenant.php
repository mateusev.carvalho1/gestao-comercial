<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 29/08/2018
 * Time: 11:30
 */

namespace App\Tenant;


use App\Http\Controllers\Api\Auth\AuthTrait;
use App\Models\Tenant;

class ManagerTenant
{
    use AuthTrait;

    public function getTenantIdentify()
    {
        $user = $this->getUser();

        if ($user['status'] != 200)
            return abort($user['status'], $user['response']);

        return $user['response']['tenant_id'];
    }

    public function getTenant()
    {
        $user = $this->getUser();
        if ($user['status'] != 200)
            return abort($user['status'], $user['response']);

        $tenant = Tenant::find($user['response']['tenant_id']);

        return $tenant;
    }

}