<?php
/**
 * Created by PhpStorm.
 * User: mateu
 * Date: 31/08/2018
 * Time: 12:32
 */

namespace App\Tenant;


use Illuminate\Database\Eloquent\Model;

class TenantObserver
{
    public function creating(Model $model)
    {
        $tenant = app(ManagerTenant::class)->getTenantIdentify();
        $model->setAttribute('tenant_id', $tenant);
    }
}