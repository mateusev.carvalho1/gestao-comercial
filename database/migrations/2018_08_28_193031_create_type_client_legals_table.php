<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeClientLegalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_client_legals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('client_id');
            $table->string('cnpj', 20)->nullable();
            $table->string('razao_social', 80)->nullable();
            $table->string('inscricao_estadual', 40)->nullable();
            $table->string('inscricao_municipal', 40)->nullable();
            $table->string('suframa', 40)->nullable();
            $table->string('tipo_contribuinte', 20)->nullable();
            $table->string('responsavel', 60)->nullable();
            $table->timestamps();

            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('cascade');

            $table->foreign('tenant_id')
                ->references('id')
                ->on('tenants')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_client_legals');
    }
}
