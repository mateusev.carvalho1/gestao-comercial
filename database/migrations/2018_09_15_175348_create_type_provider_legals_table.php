<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeProviderLegalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_provider_legals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('provider_id');
            $table->string('cnpj', 20)->nullable();
            $table->string('razao_social', 80)->nullable();
            $table->string('inscricao_estadual', 40)->nullable();
            $table->string('inscricao_municipal', 40)->nullable();
            $table->string('suframa', 40)->nullable();
            $table->string('tipo_contribuinte', 20)->nullable();
            $table->string('responsavel', 60)->nullable();
            $table->timestamps();

            $table->foreign('provider_id')
                ->references('id')
                ->on('providers')
                ->onDelete('cascade');

            $table->foreign('tenant_id')
                ->references('id')
                ->on('tenants')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_provider_legals');
    }
}
