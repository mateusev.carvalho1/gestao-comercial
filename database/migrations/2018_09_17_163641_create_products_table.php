<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('product_category_id');
            $table->unsignedInteger('unity_id');
            $table->unsignedInteger('provider_id')->nullable();
            $table->string('nome', 190);
            $table->string('codigo_interno', 30);
            $table->string('codigo_barras', 30)->nullable();
            $table->boolean('movimenta_estoque');
            $table->boolean('emitir_nf');
            $table->boolean('possui_variacao');
            $table->boolean('possui_juros');
            $table->boolean('ativo');
            $table->double('juros')->nullable();
            $table->double('peso')->nullable();
            $table->double('largura')->nullable();
            $table->double('altura')->nullable();
            $table->double('cumprimento')->nullable();
            $table->double('comissao')->nullable();
            $table->string('imagem')->nullable();
            $table->text('observacao')->nullable();
            $table->timestamps();

            $table->foreign('tenant_id')
                ->references('id')
                ->on('tenants')
                ->onDelete('cascade');

            $table->foreign('product_category_id')
                ->references('id')
                ->on('product_categories')
                ->onDelete('cascade');

            $table->foreign('unity_id')
                ->references('id')
                ->on('unities')
                ->onDelete('cascade');

            $table->foreign('provider_id')
                ->references('id')
                ->on('providers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
