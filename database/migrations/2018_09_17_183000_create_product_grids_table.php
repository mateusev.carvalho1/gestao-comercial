<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductGridsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_grids', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('product_id');
            $table->string('codigo_identificacao', 20)->nullable();
            $table->integer('estoque_minimo');
            $table->integer('estoque_maximo');
            $table->integer('estoque_atual');
            $table->double('valor_custo')->nullable();
            $table->double('valor_despesas')->nullable();
            $table->double('valor_custo_final')->nullable();
            $table->double('valor_venda');
            $table->double('porcentagem_lucro')->nullable();
            $table->timestamps();

            $table->foreign('tenant_id')
                ->references('id')
                ->on('tenants')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_grids');
    }
}
