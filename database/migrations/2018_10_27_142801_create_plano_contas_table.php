<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanoContasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plano_contas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_id');
            $table->unsignedInteger('payment_id');
            $table->unsignedInteger('bank_id');
            $table->unsignedInteger('centro_custo_id')->nullable();
            $table->string('name', 30);
            $table->string('ocorrencia', 20);
            $table->date('proximo_vencimento');
            $table->string('quantas_vezes', 20);
            $table->date('data_termino')->nullable();
            $table->integer('quantas')->nullable();
            $table->double('valor');
            $table->boolean('situacao');
            $table->text('observacao', 1000)->nullable();
            $table->timestamps();

            $table->foreign('tenant_id')
                ->references('id')
                ->on('tenants')
                ->onDelete('cascade');

            $table->foreign('payment_id')
                ->references('id')
                ->on('payments')
                ->onDelete('cascade');

            $table->foreign('bank_id')
                ->references('id')
                ->on('banks')
                ->onDelete('cascade');

            $table->foreign('centro_custo_id')
                ->references('id')
                ->on('centro_custos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plano_contas');
    }
}
