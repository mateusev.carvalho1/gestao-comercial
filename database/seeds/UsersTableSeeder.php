<?php

use Illuminate\Database\Seeder;
use App\Models\Tenant;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = Tenant::create([
            'company' => 'Upinsofts Desensenvolvimento Web',
        ]);
        $tenant->users()->create([
            'name' => 'Mateus Carvalho',
            'email' => 'mateusev.carvalho@gmail.com',
            'phone' => '31991243832',
            'password' => Hash::make('assessos'),
        ]);

        $tenant = Tenant::create([
            'company' => 'Avanti Premium',
        ]);
        $tenant->users()->create([
            'name' => 'Avanti Premium',
            'email' => 'avanti.premium@gmail.com',
            'phone' => '31991243838',
            'password' => Hash::make('assessos'),
        ]);

    }
}
