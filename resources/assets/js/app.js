require('./bootstrap');
window.Vue = require('vue');
require('./filters');

import router from './routes/routes'
import store from './vuex/store'
import Snotify from 'vue-snotify'
import VueElementLoading from 'vue-element-loading'
import PanelComponent from './components/layout/PanelComponent'
import BreadcrumbComponent from './components/layout/BreadcrumbComponent'
import flatPickr from 'vue-flatpickr-component'
import MaskedInput from 'vue-masked-input'
import Vue2Filters from 'vue2-filters'
import Money from 'v-money'
import {money} from './config'
import VTooltip from 'v-tooltip'

import {SNOTIFY_OPTIONS} from "./config";

Vue.use(Snotify, SNOTIFY_OPTIONS)
Vue.use(Vue2Filters)
Vue.use(Money, money)
Vue.use(VTooltip)
Vue.component('inputMask', MaskedInput)
Vue.component('loading', VueElementLoading)
Vue.component('PanelComponent', PanelComponent)
Vue.component('BreadcrumbComponent', BreadcrumbComponent)
Vue.component('input-date', flatPickr);
Vue.component('pagination', require('laravel-vue-pagination'));


const app = new Vue({
    el: '#app',
    router,
    store,
});
