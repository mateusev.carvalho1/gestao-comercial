import {SnotifyPosition} from 'vue-snotify'
import {Portuguese} from 'flatpickr/dist/l10n/pt'

export const URL_BASE = '/api/v1/'

export const NAME_TOKEN = 'TOKEN_AUTH'

export let SNOTIFY_OPTIONS = {
    toast: {
        position: SnotifyPosition.rightBottom
    }
}

export const CONFIG_FORM_DATE = {
    wrap: true,
    altFormat: 'd/m/Y',
    altInput: true,
    dateFormat: 'Y-m-d',
    locale: Portuguese,
}

export const money = {
    decimal: ',',
    thousands: '.',
    precision: 2,
    masked: false
}