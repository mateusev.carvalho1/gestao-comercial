import moment from 'moment'


Vue.filter('dateFormat', (value) => {
    return moment(value).format("DD/MM/Y")
})

Vue.filter('typePerson', (value) => {
    if (value == 'fisica')
        return 'Pessoa Física'
    if (value == 'juridica')
        return 'Pessoa Jurídica'
    if (value == 'estrangeiro')
        return 'Estrangeiro'
    else return null
})

Vue.filter('filterBoolean', (value) => {
    if (value)
        return 'Sim'
    return 'Não'
})

require('icheck/icheck')
Vue.directive("icheck", {
    inserted: function (el, binding) {
        jQuery(el).iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        }).on('ifChecked ifUnchecked', function () {
            $(this)[0].dispatchEvent(new Event("change"));
        })
    },
});

require('select2/dist/js/select2');
require('select2/dist/js/i18n/pt-BR');

/* Select 2 custom directive for the Vue with dynamic array based options */
function updateFunction(el, binding) {
    // get options from binding value.
    // v-select="THIS-IS-THE-BINDING-VALUE"
    let options = binding.value || {theme: 'bootstrap', language: "pt-BR"};

    // set up select2
    $(el).select2(options).on("select2:select", (e) => {
        // v-model looks for
        //  - an event named "change"
        //  - a value with property path "$event.target.value"
        el.dispatchEvent(new Event('change', {target: e.target}));
    });
}

Vue.directive('select2', {
    inserted: updateFunction,
    componentUpdated: updateFunction,
});