import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes.map'
import store from '../vuex/store'
//import Nprogress from 'nprogress'
import {NAME_TOKEN} from "../config";

Vue.use(VueRouter)


const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    const token = sessionStorage.getItem(NAME_TOKEN)

    if (to.meta.auth || to.matched.some(record => record.meta.auth)) {
        if (!token)
            return window.location.href = '/login'

        store.dispatch('checkLogin')
            .catch(() => {
                return window.location.href = '/login'
            })
    }

    next()
})

router.afterEach((to, from) => {
   // Nprogress.done()
})

export default router