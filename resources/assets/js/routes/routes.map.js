import Component404 from '../components/pages/404/404Component'
import LoginComponent from '../components/auth/LoginComponent'
import LayoutComponent from '../components/layout/LayoutComponent'
import DashboardComponent from '../components/pages/DashboardComponent'
import ClientsComponent from '../components/pages/clients/ClientsComponent'
import CreateClientsComponent from '../components/pages/clients/CreateClientComponent'
import EditClientComponent from '../components/pages/clients/EditClientComponent'
import DetailClientComponent from '../components/pages/clients/DetailClientComponent'
import ProvidersComponent from '../components/pages/providers/ProvidersComponent'
import CreateProvidersComponent from '../components/pages/providers/CreateProviderComponent'
import EditProvidersComponent from '../components/pages/providers/EditProviderComponent'
import DetailProvidersComponent from '../components/pages/providers/DetailProviderComponent'
import ProductsComponent from '../components/pages/products/ProductsComponent'
import CreateProductsComponent from '../components/pages/products/CreateProductComponent'
import EditProductComponent from '../components/pages/products/EditProductComponent'
import DetailProductComponent from '../components/pages/products/DetailProductComponent'
import UnitiesComponent from '../components/pages/unities/UnitiesComponent'
import CreateUnityComponent from '../components/pages/unities/CreateUnityComponent'
import EditUnityComponent from '../components/pages/unities/EditUnityComponent'
import GridsComponent from '../components/pages/grids/GridsComponent'
import CreateGridComponent from '../components/pages/grids/CreateGridComponent'
import EditGridComponent from '../components/pages/grids/EditGridComponent'
import ProductCategoriesComponent from '../components/pages/product-categories/ProductCategoriesComponent'
import CreateProductCategoryComponent from '../components/pages/product-categories/CreateProductCategoryComponent'
import EditProductCategoryComponent from '../components/pages/product-categories/EditProductCategoryComponent'
import BanksComponent from '../components/pages/banks/BanksComponent'
import CreateBankComponent from '../components/pages/banks/CreateBankComponent'
import EditBankComponent from '../components/pages/banks/EditBankComponent'
import PaymentsComponent from '../components/pages/payment/PaymentsComponent'
import CreatePaymentComponent from '../components/pages/payment/CreatePaymentComponent'
import EditPaymentComponent from '../components/pages/payment/EditPaymentComponent'
import CentroCustoComponent from '../components/pages/centro-custos/CentroCustosComponent'
import CreateCentroCustoComponent from '../components/pages/centro-custos/CreateCentroCustosComponent'
import EditCentroCustoComponent from '../components/pages/centro-custos/EditCentroCustoComponent'
import PlanoContasComponent from '../components/pages/plano-contas/PlanoContasComponent'
import CreatePlanoContasComponent from '../components/pages/plano-contas/CreatePlanoContasComponent'

const routes = [
    {
        path: '/',
        component: LayoutComponent,
        meta: {auth: true},
        children: [
            {path: '/', component: DashboardComponent, name: 'dashboard'},

            //Clientes
            {path: '/clientes', component: ClientsComponent, name: 'clients'},
            {path: '/cliente/cadastrar', component: CreateClientsComponent, name: 'clients.create'},
            {path: '/cliente/detalhe/:id', component: DetailClientComponent, name: 'clients.detail', props: true},
            {path: '/cliente/editar/:id', component: EditClientComponent, name: 'clients.edit', props: true},

            //Fornecedor
            {path: '/fornecedores', component: ProvidersComponent, name: 'providers'},
            {path: '/fornecedor/cadastrar', component: CreateProvidersComponent, name: 'providers.create'},
            {path: '/fornecedor/editar/:id', component: EditProvidersComponent, name: 'providers.edit', props: true},
            {
                path: '/fornecedor/detalhe/:id',
                component: DetailProvidersComponent,
                name: 'providers.detail',
                props: true
            },

            //Produtos
            {path: '/gerenciar-produtos', component: ProductsComponent, name: 'products'},
            {path: '/gerenciar-produtos/cadastrar', component: CreateProductsComponent, name: 'products.create'},
            {
                path: '/gerenciar-produtos/editar/:id',
                component: EditProductComponent,
                name: 'products.edit',
                props: true
            },
            {
                path: '/gerenciar-produtos/detalhe/:id',
                component: DetailProductComponent,
                name: 'products.detail',
                props: true
            },


            //Unidade de medidas
            {path: '/unidade-medida', component: UnitiesComponent, name: 'unities'},
            {path: '/unidade-medida/cadastrar', component: CreateUnityComponent, name: 'unities.create'},
            {path: '/unidade-medida/editar/:id', component: EditUnityComponent, name: 'unities.edit', props: true},

            {path: '/grade-variacoes', component: GridsComponent, name: 'grids'},
            {path: '/grade-variacoes/cadastrar', component: CreateGridComponent, name: 'grids.create'},
            {path: '/grade-variacoes/editar/:id', component: EditGridComponent, name: 'grids.edit', props: true},

            //Categorias
            {path: '/grupo-produtos', component: ProductCategoriesComponent, name: 'product-categories'},
            {
                path: '/grupo-produtos/cadastrar',
                component: CreateProductCategoryComponent,
                name: 'product-categories.create'
            },
            {
                path: '/grupo-produtos/editar/:id',
                component: EditProductCategoryComponent,
                name: 'product-categories.edit',
                props: true
            },

            //Banks
            {path: '/contas-bancarias', component: BanksComponent, name: 'banks'},
            {path: '/contas-bancarias/cadastrar', component: CreateBankComponent, name: 'banks.create'},
            {path: '/contas-bancarias/editar/:id', component: EditBankComponent, name: 'banks.edit', props: true},

            //Formas de pagamento
            {path: '/formas-pagamento', component: PaymentsComponent, name: 'payments'},
            {path: '/formas-pagamento/cadastrar', component: CreatePaymentComponent, name: 'payments.create'},
            {path: '/formas-pagamento/editar/:id', component: EditPaymentComponent, name: 'payments.edit', props: true},

            //Centro de custos
            {path: '/centro-custos', component: CentroCustoComponent, name: 'centro-custos'},
            {path: '/centro-custos/cadastrar', component: CreateCentroCustoComponent, name: 'centro-custos.create'},
            {
                path: '/centro-custos/editar/:id',
                component: EditCentroCustoComponent,
                name: 'centro-custos.edit',
                props: true
            },

            //Plano de Contas
            {path: '/plano-contas', component: PlanoContasComponent, name: 'plano-contas'},
            {path: '/plano-contas/cadastrar', component: CreatePlanoContasComponent, name: 'plano-contas.create'},
        ]
    },

    {path: '/login', component: LoginComponent, name: 'login', meta: {auth: false}},
    {path: '*', meta: {auth: true}, component: Component404},
]

export default routes