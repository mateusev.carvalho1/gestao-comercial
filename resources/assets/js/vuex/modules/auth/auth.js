import {NAME_TOKEN} from "../../../config";
import Nprogress from 'nprogress'

export default {
    state: {
        data: {},
        authenticated: false,
    },

    mutations: {
        AUTH_USER_OK(state, user) {
            state.authenticated = true
            state.data = user
        },

        AUTH_USER_LOGOUT(state) {
            state.authenticated = false
            state.data = {}
        },

        CHANGE_URL_BACK(state, url) {
            state.urlBack = url
        },

    },

    actions: {
        login(context, formLogin) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.post('api/login', formLogin)
                    .then(response => {
                        context.commit('AUTH_USER_OK', response.data)


                        const token = response.data.token

                        sessionStorage.setItem(NAME_TOKEN, token)
                        sessionStorage.setItem('USER_NAME', response.data.user.name)
                        window.axios.defaults.headers.common['Authorization'] = `Bearer ${token}`

                        resolve(response)
                    })
                    .catch(error => {
                        Nprogress.done()
                        reject(error.response.data)
                    })
                    .finally(() => {
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },

        logout(context) {
            return new Promise((resolve, reject) => {
                sessionStorage.removeItem(NAME_TOKEN)
                sessionStorage.removeItem('USER_NAME')
                context.commit('AUTH_USER_LOGOUT')
                resolve()
            })
        },

        checkLogin(context) {
            return new Promise((resolve, reject) => {
                const token = sessionStorage.getItem(NAME_TOKEN)
                if (!token)
                    return reject()

                axios.get('/api/me')
                    .then(response => {
                        context.commit('AUTH_USER_OK', response.data)
                        resolve(response)
                    })
                    .catch((error) => {
                        context.dispatch('logout')
                        Nprogress.done()
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                    })


            })
        }
    }
}