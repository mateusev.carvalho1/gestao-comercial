import {URL_BASE} from "../../../config";
import Nprogress from 'nprogress'

const RESOURCE = 'banks'

export default {
    state: {
        items: {
            data: []
        }
    },

    mutations: {
        GET_BANKS(state, banks) {
            state.items = banks
        }
    },

    actions: {
        loadBanks(context) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}`)
                    .then((response) => {
                        context.commit('GET_BANKS', response.data.banks)
                        resolve(response.data.banks)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        storeBank(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.post(`${URL_BASE}${RESOURCE}`, formData)
                    .then((response) => resolve(response))
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },


        loadBank(context, id) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        updateBank(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.put(`${URL_BASE}${RESOURCE}/${formData.id}`, formData)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },

        destroyBank(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        Nprogress.done()
                        resolve(response)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        Nprogress.done()
                        reject(error)
                    })
            })
        }

    },
}