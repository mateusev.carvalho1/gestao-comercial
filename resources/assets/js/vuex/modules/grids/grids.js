import {URL_BASE} from "../../../config";
import Nprogress from 'nprogress'

const RESOURCE = 'grids'

export default {
    state: {
        items: {
            data: []
        }
    },

    mutations: {
        GET_GRIDS(state, grids) {
            state.items = grids
        }
    },

    actions: {
        loadGrids(context) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}`)
                    .then((response) => {
                        context.commit('GET_GRIDS', response.data.grids)
                        resolve(response.data.grids)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        storeGrid(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.post(`${URL_BASE}${RESOURCE}`, formData)
                    .then((response) => resolve(response))
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },


        loadGrid(context, id) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        updateGrid(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.put(`${URL_BASE}${RESOURCE}/${formData.id}`, formData)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },

        destroyGrid(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        Nprogress.done()
                        resolve(response)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        Nprogress.done()
                        reject(error)
                    })
            })
        }

    },
}