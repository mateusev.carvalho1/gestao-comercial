import {URL_BASE} from "../../../config";
import Nprogress from 'nprogress'

const RESOURCE = 'plano-contas'

export default {
    state: {
        items: {
            data: []
        }
    },

    mutations: {
        GET_PLANO_CONTAS(state, planoContas) {
            state.items = planoContas
        }
    },

    actions: {
        loadPlanoContas(context, params) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}`, {params})
                    .then((response) => {
                        context.commit('GET_PLANO_CONTAS', response.data.planoContas)
                        resolve(response.data.planoContas)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        storePlanoConta(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.post(`${URL_BASE}${RESOURCE}`, formData)
                    .then((response) => resolve(response))
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },


        loadPlanoConta(context, id) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        updatePlanoConta(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.put(`${URL_BASE}${RESOURCE}/${formData.id}`, formData)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },

        destroyPlanoConta(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        Nprogress.done()
                        resolve(response)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        Nprogress.done()
                        reject(error)
                    })
            })
        }

    },
}