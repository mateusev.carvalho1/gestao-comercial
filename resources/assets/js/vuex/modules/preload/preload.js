export default {
    state: {
        loading: false,
        fullLoading: false,
        btnLoading: false,
    },

    mutations: {
        CHANGE_LOAD(state, status) {
            state.loading = status
        },
        CHANGE_BTN_LOAD(state, status) {
            state.btnLoading = status
        },
        CHANGE_FULL_LOAD(state, status) {
            state.fullLoading = status
        },
    }
}