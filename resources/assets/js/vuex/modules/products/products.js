import {URL_BASE} from "../../../config";
import Nprogress from 'nprogress'

const RESOURCE = 'products'
const CONFIGS = {
    headers: {
        'content-type': 'multipart/form-data'
    }
}

export default {
    state: {
        items: {
            data: [],
        }
    },

    mutations: {
        GET_PRODUCTS(state, products) {
            state.items = products
        }
    },

    actions: {
        loadProducts(context, params) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}`, {params})
                    .then((response) => {
                        context.commit('GET_PRODUCTS', response.data.products)
                        resolve(response.data.products)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        storeProduct(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.post(`${URL_BASE}${RESOURCE}`, formData, CONFIGS)
                    .then((response) => resolve(response))
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },

        loadProduct(context, id) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        updateProduct(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                formData.append('_method', 'PUT')
                axios.post(`${URL_BASE}${RESOURCE}/${formData.get('id')}`, formData)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },

        destroyProduct(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        Nprogress.done()
                        resolve(response)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        Nprogress.done()
                        reject(error)
                    })
            })
        }

    }
}