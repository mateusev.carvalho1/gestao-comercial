import {URL_BASE} from "../../../config";
import Nprogress from 'nprogress'

const RESOURCE = 'providers'

export default {
    state: {
        items: {
            data: [],
        }
    },

    mutations: {
        GET_PROVIDERS(state, providers) {
            state.items = providers
        }
    },

    actions: {
        loadProviders(context, params) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}`, {params})
                    .then((response) => {
                        context.commit('GET_PROVIDERS', response.data.providers)
                        resolve(response.data.providers)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        storeProvider(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.post(`${URL_BASE}${RESOURCE}`, formData)
                    .then((response) => resolve(response))
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },

        loadProvider(context, id) {
            context.commit('CHANGE_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.get(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_LOAD', false)
                    })
            })
        },

        updateProvider(context, formData) {
            context.commit('CHANGE_BTN_LOAD', true)
            return new Promise((resolve, reject) => {
                axios.put(`${URL_BASE}${RESOURCE}/${formData.id}`, formData)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch((error) => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        reject(error)
                    })
                    .finally(() => {
                        Nprogress.done()
                        context.commit('CHANGE_BTN_LOAD', false)
                    })
            })
        },

        destroyProvider(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete(`${URL_BASE}${RESOURCE}/${id}`)
                    .then(response => {
                        Nprogress.done()
                        resolve(response)
                    })
                    .catch(error => {
                        if (error.response.data.message == 'Unauthenticated.') {
                            context.dispatch('logout')
                            window.location.href = '/login'
                        }
                        Nprogress.done()
                        reject(error)
                    })
            })
        }

    }
}