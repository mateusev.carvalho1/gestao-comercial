import {NAME_TOKEN} from "../../../config";

export default {
    state: {},

    mutations: {},

    actions: {
        loadAddress(context, params) {
            delete axios.defaults.headers.common["X-CSRF-TOKEN"]
            delete axios.defaults.headers.common["Authorization"]

            axios.get(`https://viacep.com.br/ws/${params}/json/`)
            return new Promise((resolve, reject) => {
                axios.get(`https://viacep.com.br/ws/${params}/json/`)
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(error => {
                        reject(error)
                    })
                    .finally(() => {
                        let token = document.head.querySelector('meta[name="csrf-token"]');
                        if (token) {
                            window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
                        }
                        const tokenAuth = sessionStorage.getItem(NAME_TOKEN)
                        if (tokenAuth)
                            window.axios.defaults.headers.common['Authorization'] = `Bearer ${tokenAuth}`;
                    })
            })
        }
    }
}