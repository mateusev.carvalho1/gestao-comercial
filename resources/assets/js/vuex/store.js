import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth/auth'
import preload from './modules/preload/preload'
import clients from './modules/clients/clients'
import vendors from './modules/vendors/vendors'
import providers from './modules/providers/providers'
import unities from './modules/unities/unities'
import grids from './modules/grids/grids'
import productCategories from './modules/product-categories/productCategories'
import products from './modules/products/products'
import banks from './modules/banks/banks'
import payments from './modules/payments/payments'
import centroCustos from './modules/centro-custos/CentroCustos'
import planoContas from './modules/placo-contas/planoContas'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        auth,
        preload,
        clients,
        vendors,
        providers,
        unities,
        grids,
        productCategories,
        products,
        banks,
        payments,
        centroCustos,
        planoContas,
    }
})

export default store