<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Gestão Comercial - Autenticação</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{mix('css/app.css')}}">

</head>
<body class="gray-bg">
<div id="app">
    <vue-snotify></vue-snotify>
    @yield('content')
</div>
<script src="{{mix('/js/app.js')}}"></script>
</body>
</html>
