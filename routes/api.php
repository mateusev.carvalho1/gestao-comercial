<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Rotas de autenticação
 */
$this->group(['namespace' => 'Api\Auth',], function () {
    $this->post('login', 'AuthController@login');
    $this->get('me', 'AuthController@getAuthenticatedUser');

    $this->post('create-user', 'ProfileController@create');
    $this->put('update-user', 'ProfileController@update');
});

$this->group([
    'prefix' => 'v1',
    'namespace' => 'Api\v1',
    'middleware' => ['auth:api', 'tenant.uuid']
], function () {
    $this->apiResource('clients', 'ClientController');
    $this->apiResource('providers', 'ProviderController');
    $this->apiResource('unities', 'UnityController');
    $this->apiResource('unities', 'UnityController');
    $this->apiResource('grids', 'GridController');
    $this->apiResource('product-categories', 'ProductCategoryController');
    $this->apiResource('products', 'ProductController');
    $this->apiResource('banks', 'BankController');
    $this->apiResource('payments', 'PaymentController');
    $this->apiResource('centro-custos', 'CentroCustoController');
    $this->apiResource('plano-contas', 'PlanoContaController');
});


