<?php

Route::view('/login', 'vue.login');

Route::get('/{vue_capture?}', function () {
    return view('vue.index');
})->where('vue_capture', '[\/\w\.-]*');
